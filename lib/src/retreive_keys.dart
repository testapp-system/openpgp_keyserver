import 'package:http/http.dart' as http;

/*
        Default keyservers (HTTPS and CORS enabled)
    */
const DEFAULT_KEYSERVERS = [
  "https://keys.fedoraproject.org/",
  "https://keys.openpgp.org/",
];

class PublicKey {
/*
        Initialization to create an PublicKey object.
        Arguments:
        * keyservers - Array of keyserver domains, default is:
            ["https://keys.fedoraproject.org/", "https://keybase.io/"]
        Examples:
        //Initialize with the default keyservers
        var hkp = new PublicKey();
        //Initialize only with a specific keyserver
        var hkp = new PublicKey(["https://key.ip6.li/"]);
    */
  final List<String> keyservers;

  PublicKey({this.keyservers = DEFAULT_KEYSERVERS});

/*
        Get a public key from any keyserver based on keyId.
        Arguments:
        * keyId - String key id of the public key (this is usually a fingerprint)
        * callback - Function that is called when finished. Two arguments are
                passed to the callback: publicKey and errorCode. publicKey is
                an ASCII armored OpenPGP public key. errorCode is the error code
                (either HTTP status code or keybase error code) returned by the
                last keyserver that was tried. If a publicKey was found,
                errorCode is null. If no publicKey was found, publicKey is null
                and errorCode is not null.
        Examples:
        //Get a valid public key
        var hkp = new PublicKey();
        hkp.get("F75BE4E6EF6E9DD203679E94E7F6FAD172EFEE3D", function(publicKey, errorCode){
            errorCode !== null ? console.log(errorCode) : console.log(publicKey);
        });
        //Try to get an invalid public key
        var hkp = new PublicKey();
        hkp.get("bogus_id", function(publicKey, errorCode){
            errorCode !== null ? console.log(errorCode) : console.log(publicKey);
        });
    */
  Future<String> get({String keyId}) async {
    for (int i = 0; i < keyservers.length; i++) {
//set the keyserver to try next
      var ks = keyservers[i];

//add the 0x prefix if absent
      if (!keyId.startsWith("0x")) {
        keyId = "0x" + keyId;
      }

//request the public key from the hkp server
      final response =
          await http.get(ks + "pks/lookup?op=get&options=mr&search=" + keyId);
      if (response.statusCode == 200) return response.body;
    }
    throw KeyNotFoundError();
  }

/*
        Search for a public key in the keyservers.
        Arguments:
        * query - String to search for (usually an email, name, or username).
        * callback - Function that is called when finished. Two arguments are
                passed to the callback: results and errorCode. results is an
                Array of users that were returned by the search. errorCode is
                the error code (either HTTP status code or keybase error code)
                returned by the last keyserver that was tried. If any results
                were found, errorCode is null. If no results are found, results
                is null and errorCode is not null.
        Examples:
        //Search for diafygi's key id
        var hkp = new PublicKey();
        hkp.search("diafygi", function(results, errorCode){
            errorCode !== null ? console.log(errorCode) : console.log(results);
        });
        //Search for a nonexistent key id
        var hkp = new PublicKey();
        hkp.search("doesntexist123", function(results, errorCode){
            errorCode !== null ? console.log(errorCode) : console.log(results);
        });
    */
  Future<List> search({
    query,
  }) async {
    List ks_results = [];
    for (int i = 0; i < keyservers.length; i++) {
//set the keyserver to try next
      var ks = this.keyservers[i];

//normal HKP keyserver
      final result = await http.get(ks +
          "pks/lookup?op=index&options=mr&fingerprint=on&search=" +
          Uri.encodeComponent(query));
      if (result.statusCode == 200) {
        var raw = result.body.split("\n");
        var curKey = null;
        for (int i = 0; i < raw.length; i++) {
          var line = raw[i].trim();

//pub:<keyid>:<algo>:<keylen>:<creationdate>:<expirationdate>:<flags>
          if (line.indexOf("pub:") == 0) {
            if (curKey != null) {
              ks_results.add(curKey);
            }
            var vals = line.split(":");
            curKey = {
              "keyid": vals[1],
              "href": ks + "pks/lookup?op=get&options=mr&search=0x" + vals[1],
              "info": ks + "pks/lookup?op=vindex&search=0x" + vals[1],
              "algo": vals[2] == "" ? null : int.parse(vals[2]),
              "keylen": vals[3] == "" ? null : int.parse(vals[3]),
              "creationdate": vals[4] == "" ? null : int.parse(vals[4]),
              "expirationdate": vals[5] == "" ? null : int.parse(vals[5]),
              "revoked": vals[6].contains("r"),
              "disabled": vals[6].contains("d"),
              "expired": vals[6].contains("e"),
              "uids": [],
            };
          }

//uid:<escaped uid string>:<creationdate>:<expirationdate>:<flags>
          if (line.indexOf("uid:") == 0) {
            var vals = line.split(":");
            curKey['uids'].push({
              "uid": Uri.decodeComponent(vals[1]),
              "creationdate": vals[2] == "" ? null : int.parse(vals[2]),
              "expirationdate": vals[3] == "" ? null : int.parse(vals[3]),
              "revoked": vals[4].contains("r"),
              "disabled": vals[4].contains("d"),
              "expired": vals[4].contains("e"),
            });
          }
        }
        if (curKey != null) {
          ks_results.add(curKey);
        }
      }
    }
    return ks_results;
  }
}

class KeyNotFoundError implements Exception {}
